module.exports = {
    entry: "./src/entry.js",
    devtool: "eval",
    output: {
        path: __dirname,
        filename: "dist/st-ui-htmleditor.js",
        library: "ST_UI_HTMLEDITOR",
        libraryTarget: "umd"
    },
    module: {
        loaders: [{
            test: /\.(eot|svg|ttf|woff|woff2)$/,
            loader: "url-loader?name=dist/[name].[ext]"
        }, {
            test: /\.(eot|svg|ttf|woff|woff2)$/,
            loader: "file-loader?name=dist/[name].[ext]"
        }, {
            test: /\.css$/,
            loader: "style-loader!css-loader"
        }]
    }
};
