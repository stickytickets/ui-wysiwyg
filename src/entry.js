if (document.all && !document.addEventListener) {
    require('redactor-ltie9');
    require('./vendor/css/redactor-ltie9.css');
} else {
    require('redactor');
    require('./vendor/css/redactor.css');
}

jQuery.fn.chtmleditor = function(options) {
    var className = 'ui-htmleditor';
    var defaults = {
        buttonSource: false,
        source: false,
        toolbarFixed: false,
        fixed: false, /* old redactor setting name*/
        uploadCrossDomain: true,
        removeEmptyTags: false, /* redactory 10_+ removeEmpty: ['strong', 'em', 'span'], */
        deniedTags: ['html', 'head', 'link', 'body', 'meta', 'script', 'style', 'applet', 'form'],
        plugins: ['fontcolor', 'fontsize'],
        imageUploadErrorCallback: function () {
            alert('An error occurred uploading your image, please check the image format or contact support');
        },
        uploadFields: {
            NCSRF: jQuery.cookie('NCSRF')
        },
        dragUpload: false
    };

    return this.each(function() {
        jQuery(this).addClass(className).redactor(jQuery.extend(defaults, options));
        jQuery(this).data('chtmleditor', jQuery(this).data('redactor'));
    });
};

exports.mount = function(el) {
    window.jQuery(el).chtmleditor();
};
